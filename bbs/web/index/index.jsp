<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>

    <title>Welcome To BBS</title>
    <base href="http://localhost:8080/bbs/">
    <link type="text/css" rel="stylesheet" href="static/css/style.css" >
</head>
<body>

<div class="h">
    <h1 align="center">校园BBS</h1>
</div>
<div align="right">

    <c:if test="${empty sessionScope.user}">
        <div>
            <center><h1></h1></center>
            <marquee bgcolor="blue" behavior="scroll">
                <font color="white" size="+1">登录后才可以发帖哦</font>
            </marquee>
        </div>
        <a href="pages/user/login.jsp">登录</a>&nbsp;| &nbsp;<A href="pages/user/regist.jsp">注册</A>|
    </c:if>
    <c:if test="${not empty sessionScope.user.userName}">
        <span> 当前用户:[${sessionScope.user.userName}]</span>
        <a href="pages/user/index.jsp">个人中心</a>&nbsp;
    </c:if>
</div>
<hr>

<div id="main">
    <h1>论坛话题区:</h1>

    <table align="center" width="400" rules="rows" >
        <tr>
            <td >模块号</td>
            <td>主题名</td>
            <td >操作</td>
        </tr>
        <c:forEach items="${requestScope.board}" var="board">
            <tr>
                <td>${board.boardId}</td>
                <td>${board.boardName}</td>
                <td><a href="topicServlet?action=queryTopic&boardId=${board.boardId}">查看</a></td>
                <c:if test="${not empty sessionScope.user.userName}">
                <td><a href="topicServlet?action=addTopic&boardId=${board.boardId}">发帖</a></td>
                </c:if>
            </tr>

        </c:forEach>

    </table>




</body>
</html>