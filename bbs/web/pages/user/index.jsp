<%--<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>欢迎访问校园BBS</title>
    <base href="http://localhost:8080/bbs/">
    <link type="text/css" rel="stylesheet" href="static/css/style.css" >
</head>
<body>

<div class="h">
    <h1 align="center">${sessionScope.user.userName}的个人主页</h1>
</div>
<div align="right">

        <a href="boardServlet?action=queryBoard">[论坛]</a>
       <a href="userServlet?action=logout">[退出]</a>

</div>



<br><HR>
<div class="b_list">
    <div class="img_div">
        <img class="user_img" alt="" src="${sessionScope.user.head}" />
    </div>
    <div class="user_info">
        <div class="user_name">
            <span class="sp1">用户ID：</span>
            <span class="sp2">${sessionScope.user.userId}</span>
        </div>
        <br/>
        <br/>
        <div class="user_name">
            <span class="sp1">用户名:</span>
            <span class="sp2">${sessionScope.user.userName}</span>
        </div>
        <br/>
        <br/>
        <div class="user_gender">
            <span class="sp1">性别:</span>
            <span class="sp2">${sessionScope.user.gender}</span>
        </div>
        <br/>
        <br/>
        <div class="user_gender">
            <span class="sp1">注册时间:</span>
            <span class="sp2">${sessionScope.user.regTime}</span>
        </div>

    </div>
</div>


</body>
</html>
