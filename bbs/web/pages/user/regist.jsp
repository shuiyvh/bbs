<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
	<title>校园BBS--注册</title>
	<base href="http://localhost:8080/bbs/">
	<link type="text/css" rel="stylesheet" href="static/css/style.css" >
	<script type="text/javascript" src="static/script/jquery-1.7.2.js"></script>
	<script type="text/javascript">


		// 页面加载完成之后
		$(function (){

			//给注册绑定单击事件
			$("#sub_btn").click(function (){
				//验证用户名：必须由字母数字下划线组成，长度5-12
				//1。获取用户名输入框的内容
				var usernameText = $("#username").val();
				//2。创建正则表达式对象
				var usernamePatt = /^\w{5,12}$/;
				//3。使用test方法验证
				if(!usernamePatt.test(usernameText)){
					//4。提示用户结果
					$("span.errorMsg").text("用户名不合法");

					return false;
				}

				//验证密码：必须由字母数字下划线组成，长度5-12
				//1。获取用户名输入框的内容
				var passwordText = $("#password").val();
				//2。创建正则表达式对象
				var passwordPatt = /^\w{5,12}$/;
				//3。使用test方法验证
				if(!passwordPatt.test(passwordText)){
					//4。提示用户结果
					$("span.errorMsg").text("密码不合法");

					return false;
				}
				//验证确认密码：和密码相同
				//1。获取密码内容
				var repwdText = $("#repwd").val();
				//2。和密码相比较
				if(repwdText != passwordText){
					//3。提示用户
					$("span.errorMsg").text("密码确认失败！");
					return false;
				}


				$("span.errorMsg").text(); // 验证成功取消页面不合法提示
				alert("注册成功！");
			});
		});

	</script>
</head>
<body>

<!--      用户信息、登录、注册        -->

<div class="h">
	<h1 align="center">校园BBS</h1>
</div>


<br/>
<!--      导航        -->
<div>
	&gt;&gt;<B><a href="index.jsp">首页</a></B>
</div>

<!--      用户注册表单        -->
<div  class="t" style="MARGIN-TOP: 15px" align="center">
	<div align="right">
		已有账号？--><A href="pages/user/login.jsp">登录</A>|
	</div>
	<span class="errorMsg">
		${requestScope.msg}
	</span>
	<form  action="userServlet" method="post">
		<input type="hidden" name="action" value="regist">

		<label>用&nbsp;户&nbsp;名&nbsp;:</label>
		<INPUT class="itxt"  type="text" placeholder="请输入用户名" autocomplete="off" tabindex="1" name="username"
			   id="username">
		<br/>
		<br/>
		<label>密&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;码:</label>
		<input class="itxt" type="password" placeholder="请输入密码" autocomplete="off" tabindex="1" name="password" id="password"
		value="${requestScope.password}"/>
		<br/>
		<br/>
		<label>&nbsp;重复密码:</label>
		<input class="itxt" type="password" placeholder="确认密码" autocomplete="off" tabindex="1" name="repwd" id="repwd"
			   value="${requestScope.password}" />
		<br/>
		<br/>
		<label>性&nbsp;&nbsp;&nbsp;别 &nbsp;:</label>
		女<input type="radio" name="gender" value="1">
		男<input type="radio" name="gender" value="2" checked="checked" />
		<br/>
		<br/>
		<label>请选择头像 </label><br/>
		<img src="image/head/1.gif"/><input type="radio" name="head" value="image/head/1.gif" checked="checked">
		<img src="image/head/2.gif"/><input type="radio" name="head" value="image/head/2.gif">
		<img src="image/head/3.gif"/><input type="radio" name="head" value="image/head/3.gif">
		<img src="image/head/4.gif"/><input type="radio" name="head" value="image/head/4.gif">
		<img src="image/head/5.gif"/><input type="radio" name="head" value="image/head/5.gif">
		<BR/>
		<img src="image/head/6.gif"/><input type="radio" name="head" value="image/head/6.gif">
		<img src="image/head/7.gif"/><input type="radio" name="head" value="image/head/7.gif">
		<img src="image/head/8.gif"/><input type="radio" name="head" value="image/head/8.gif">
		<img src="image/head/9.gif"/><input type="radio" name="head" value="image/head/9.gif">
		<img src="image/head/10.gif"/><input type="radio" name="head" value="image/head/10.gif">
		<BR/>
		<img src="image/head/11.gif"/><input type="radio" name="head" value="image/head/11.gif">
		<img src="image/head/12.gif"/><input type="radio" name="head" value="image/head/12.gif">
		<img src="image/head/13.gif"/><input type="radio" name="head" value="image/head/13.gif">
		<img src="image/head/14.gif"/><input type="radio" name="head" value="image/head/14.gif">
		<img src="image/head/15.gif"/><input type="radio" name="head" value="image/head/15.gif">
		<br/>
		<input class="btn" id="sub_btn" type="submit" value="注 册">


	</form>

</div>
<br/>
</body>
</html>
