<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>

    <title>帖子详情</title>
    <base href="http://localhost:8080/bbs/">
    <link type="text/css" rel="stylesheet" href="static/css/style.css" >
</head>
<body>

<div class="h">
    <h1 align="center">校园BBS</h1>
</div>
<div align="right">

    <c:if test="${empty sessionScope.user}">
        <div>
            <center><h1></h1></center>
            <marquee bgcolor="blue" behavior="scroll">
                <font color="white" size="+1">登录后才可以回帖哦</font>
            </marquee>
        </div>
        <a href="pages/user/login.jsp">登录</a>&nbsp;| &nbsp;<A href="pages/user/regist.jsp">注册</A>|
    </c:if>
    <c:if test="${not empty sessionScope.user.userName}">
        <span> 当前用户:[${sessionScope.user.userName}]</span>
        <a href="pages/user/index.jsp">个人中心</a>&nbsp;
    </c:if>
</div>
<hr>

<div id="main">
    <c:forEach items="${requestScope.usertopic}" var="usertopic">
    <div class="comment-wrap">
        <div class="comment-block">
            <div class="bottom-comment">
                <div class="comment-date" align="right">作者Id:${usertopic.userId}</div>
            <div class="comment-date" align="left">原帖发布时间:${usertopic.publishTime}</div>
            </div>
            <p class="comment-text">${usertopic.content}</p>
            <div class="bottom-comment">

               <div class="comment-date" align="right">最近修改时间: ${usertopic.modifyTime}</div>
            </div>
        </div>
    </div>
        <div>
        <a href="replyServlet?action=queryReplyByTopicId&topicId=${usertopic.topicId}">查看历史回复</a>
        </div>
    <c:if test="${not empty sessionScope.user}" >
    <hr/>
    <div class="comment-wrap">
        <div class="comment-block">
            <form action="replyServlet" method="post">
               <input type="hidden" name="action" value="addReply">
              <input type="hidden" name="title" value="${usertopic.title}">
                <input type="hidden" name="userId" value="${sessionScope.user.userId}">
                <input type="hidden" name="topicId" value="${usertopic.topicId}">
                <textarea name="context" id="" cols="30" rows="5" placeholder="发表我的回复"></textarea>
                <input align="center" type="submit" value="回复"/>
            </form>
        </div>
    </div>
    </c:if>
    </c:forEach>


</div>


</body>
</html>
