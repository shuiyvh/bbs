<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>

    <title>发布新帖</title>
    <base href="http://localhost:8080/bbs/">
    <link type="text/css" rel="stylesheet" href="static/css/style.css" >
</head>
<body>

<div class="h">
    <h1 align="center">校园BBS</h1>
</div>
<div align="right">

    <c:if test="${empty sessionScope.user}">
        <a href="pages/user/login.jsp">登录</a>&nbsp;| &nbsp;<A href="pages/user/regist.jsp">注册</A>|
    </c:if>
    <c:if test="${not empty sessionScope.user.userName}">
        <span> 当前用户:[${sessionScope.user.userName}]</span>
        <a href="pages/user/index.jsp">个人中心</a>&nbsp;
    </c:if>
</div>
<hr>

<div id="main">

<div class="comment-wrap" align="center">
    <div class="comment-block">
        <form action="topicServlet" method="post">
            <input type="hidden" name="action" value="getTopic">
            <input type="hidden" name="userId" value="${sessionScope.user.userId}">
            <input type="hidden" name="boardId" value="${param.get("boardId")}">
            <br/>
            <br/>
          <div>
          新帖标题:
          </div>
            <span>
            <textarea name="title"  cols="100" rows="2" placeholder="不超过50字"></textarea>
            </span>
            <br>
            <div>
            新帖内容:
            </div>
            <span>
            <textarea name="content"  cols="100" rows="5" placeholder="不超过1000字"></textarea>
                </span>
            <br/>
            <button> <input type="submit" value="发表"></button>

        </form>
    </div>
</div>

</div>

</body>
</html>
