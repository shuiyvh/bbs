<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>

    <title>欢迎访问校园BBS首页</title>
    <base href="http://localhost:8080/bbs/">
    <link type="text/css" rel="stylesheet" href="static/css/style.css" >
</head>
<body>

<div class="h">
    <h1 align="center">校园BBS</h1>
</div>
<div align="right">

    <c:if test="${empty sessionScope.user}">
        <a href="pages/user/login.jsp">登录</a>&nbsp;| &nbsp;<A href="pages/user/regist.jsp">注册</A>|
    </c:if>
    <c:if test="${not empty sessionScope.user.userName}">
        <span> 当前用户:[${sessionScope.user.userName}]</span>
        <a href="pages/user/index.jsp">个人中心</a>&nbsp;
    </c:if>
</div>
<hr>

<div id="main">

    <h1>[${requestScope.boardName}]话题区:</h1>
    <hr/>
    <br/>

    <c:if test="${empty requestScope.queryTopic}">
    <div align="center">
        <span>该话题区还未有人发帖！！！<br/> <a href="#">快去发帖吧</a></span>
    </div>
</c:if>

    <c:if test="${not empty requestScope.queryTopic}">
    <table align="center" width="600" rules="rows" >
        <tr>
            <td >标题</td>
<%--            <td>内容</td>--%>
            <td>发布时间</td>
            <td>修改时间</td>
            <td>作者</td>
            <td>操作</td>

        <c:forEach items="${requestScope.queryTopic}" var="topics">
            <tr>
                <td>${topics.title}</td>
<%--                <td>${topics.content}</td>--%>
                <td>${topics.publishTime}</td>
                <td>${topics.modifyTime}</td>
                <td>${topics.userId}</td>
                <td><a href="topicServlet?action=queryTopicByUserId&topicId=${topics.topicId}">阅览</a></td>
            </tr>
        </c:forEach>


    </table>
    </c:if>
</div>


</body>
</html>
