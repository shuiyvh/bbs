<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>

    <title>发布新帖</title>
    <base href="http://localhost:8080/bbs/">
    <link type="text/css" rel="stylesheet" href="static/css/style.css" >
</head>
<body>

<div class="h">
    <h1 align="center">贝壳BBS论坛</h1>
</div>
<div align="right">

    <c:if test="${empty sessionScope.user}">
        <a href="pages/user/login.jsp">登录</a>&nbsp;| &nbsp;<A href="pages/user/regist.jsp">注册</A>|
    </c:if>
    <c:if test="${not empty sessionScope.user.userName}">
        <span> 当前用户:[${sessionScope.user.userName}]</span>
        <a href="pages/user/index.jsp">个人中心</a>&nbsp;
    </c:if>
</div>
<hr>

<div id="main">

        <c:forEach items="${requestScope.reply}" var="replys">
    <div class="comment-wrap">
    <div class="comment-block">
    <div class="bottom-comment">
        <div class="comment-date" align="left">回帖人Id:${replys.userId}</div>
    </div>
        <p class="comment-text">${replys.context}</p>
    <div class="bottom-comment">
        <div class="comment-date" align="right">最近回帖时间:${replys.modifyTime}</div>
    </div>
    </div>
    </div>
        </c:forEach>

</div>

</body>
</html>
