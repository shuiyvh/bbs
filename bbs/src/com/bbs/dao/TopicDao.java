package com.bbs.dao;

import com.bbs.entity.Topic;

import java.util.List;

public interface TopicDao {

    /**
     * 查询某一版块的所有帖子
     * @param boardId Integer 版块id
     * @return 该板块下所有帖子
     */
    public List<Topic> queryTopic(Integer boardId);

    /**
     * 发布帖子
     */
    public int addTopic(Topic topic);

    /**
     *  修改帖子
     */
    public int updateTopic(Topic topic);

    /**
     * 通过帖子id和发布者id删除帖子
     * @param topicId Integer 帖子id
     * @param userId Integer 发布者ID
     */
    public int deleteTopic(Integer topicId,Integer userId);

    /**
     *
     * @param userId Integer 发布者id
     * @return 该id下所有帖子
     */
    public List<Topic> queryTopicByUserId(Integer userId);

    /**
     * 查看帖子
     * @param topicId 帖子id
     * @return 该id帖子的详情
     */
    public List<Topic> queryTopicByTopicId(Integer topicId);

}
