package com.bbs.dao.impl;

import com.bbs.dao.ReplyDao;
import com.bbs.entity.Reply;

import java.util.List;

public class ReplyDaoImpl extends BaseDao implements ReplyDao {


    @Override
    public int addReply(Reply reply) {
        String sql = "insert into t_reply(title,context,ublishTime,modifyTime,userId,topicId) values(?,?,?,?,?,?)";
        return update(sql,reply.getTitle(),reply.getContext(),reply.getUblishTime(),reply.getModifyTime(),
                reply.getUserId(),reply.getTopicId());
    }

    @Override
    public List<Reply> queryReplyBytopicId(Integer topicId) {
        String sql = "select replyId,title,context,ublishTime,modifyTime,userId,topicId from t_reply where topicId = ?";
        return queryList(Reply.class,sql,topicId);
    }

    @Override
    public List<Reply> queryReplyByuserId(Integer userId) {
        String sql = "select replyId,title,context,ublishTime,modifyTime,userId,topicId from t_reply where userId = ?";
        return queryList(Reply.class,sql,userId);
    }

    @Override
    public int deleteReply(Integer replyId, Integer userId) {
        String sql = "delete from t_reply where replyId = ? and userId = ?";
        return update(sql,replyId,userId);
    }


}
