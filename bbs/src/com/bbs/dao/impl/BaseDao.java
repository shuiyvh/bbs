package com.bbs.dao.impl;

import com.bbs.utils.JdbcUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.*;
import java.util.List;

public abstract class BaseDao {
    private QueryRunner queryRunner = new QueryRunner();

    /**
     *  update方法可以用来执行增删改操作
     * @param sql  String型的sql语句
     * @param args  sql语句中的关键字参数
     * @return  返回成功的行数
     */
    public int update(String sql,Object...args){
        //先连接数据库
        Connection connection = JdbcUtils.getConnection();
        try {
            // 连接成功执行响应sql语句
            return queryRunner.update(connection,sql,args);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            // 最后关闭连接释放资源
            JdbcUtils.close(connection);
        }
        return 0;
    }

    /**
     * 查询一个javaBean的sql语句
     * @param type 返回对象的类型 对应javaBean类型
     * @param sql  sql语句
     * @param args  sql参数
     * @param <T>   返回类型的泛型，接受所有类型
     * @return 成功返回javaBean对象，失败为null
     */
    public <T> T queryone(Class<T> type,String sql,Object...args){
        Connection connection = JdbcUtils.getConnection();

        try {
            return queryRunner.query(connection,sql,new BeanHandler<T>(type),args);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            JdbcUtils.close(connection);
        }
        return null;
    }

    /**
     * 查询返回多个javaBean对象
     * @param type 返回的对象类型
     * @param sql  sql语句
     * @param args  sql参数
     * @param <T>  返回对象的泛型
     * @return  成功为对象，失败为null
     */
    public <T> List<T> queryList(Class<T> type, String sql, Object...args){
        Connection connection = JdbcUtils.getConnection();
        try {
            return queryRunner.query(connection,sql,new BeanListHandler<T>(type),args);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            JdbcUtils.close(connection);
        }
        return null;
    }

    /**
     * 执行返回一行一列的sql语句
     * @param sql 执行的sql语句
     * @param args sql对应的参数值
     * @return 失败为null
     */
    public Object queryForSingleValue(String sql,Object...args){
        Connection conn = JdbcUtils.getConnection();

        try {
            return queryRunner.query(conn,sql,new ScalarHandler(),args);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            JdbcUtils.close(conn);
        }
        return null;
    }
}
