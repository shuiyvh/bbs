package com.bbs.dao.impl;

import com.bbs.dao.TopicDao;
import com.bbs.entity.Topic;

import java.util.List;

public class TopicDaoImpl extends BaseDao implements TopicDao {
    @Override
    public List<Topic> queryTopic(Integer boardId) {
        String sql = "select topicId,title,content,publishTime,modifyTime,userId,boardId from t_topic where boardId =" +
                " ?";
        return queryList(Topic.class,sql,boardId);
    }

    @Override
    public int addTopic(Topic topic) {
        String sql = "insert into t_topic(title,content,publishTime,modifyTime,userId,boardId) values(?,?,?,?,?,?)";
        return update(sql,topic.getTitle(),topic.getContent(),topic.getPublishTime(),topic.getModifyTime(),
                topic.getUserId(),topic.getBoardId());
    }

    @Override
    public int updateTopic(Topic topic) {
        String sql = "update t_topic set title=?,content=?,modifyTime=? where topicId = ? and userId = ?";
        return update(sql,topic.getTitle(),topic.getContent(),topic.getModifyTime(),topic.getTopicId(),topic.getUserId());
    }

    @Override
    public int deleteTopic(Integer topicId, Integer userId) {

        String sql = "delete from t_topic where topicId = ? and userId = ?";
        return update(sql,topicId,userId);
    }

    @Override
    public List<Topic> queryTopicByUserId(Integer userId) {
        String sql = "select topicId,title,content,publishTime,modifyTime,userId,boardId from t_topic where userId = ?";
        return queryList(Topic.class,sql,userId);
    }

    @Override
    public List<Topic> queryTopicByTopicId(Integer topicId) {
        String sql = "select topicId,title,content,publishTime,modifyTime,userId,boardId from t_topic where topicId = ?";
        return queryList(Topic.class,sql,topicId);
    }
}
