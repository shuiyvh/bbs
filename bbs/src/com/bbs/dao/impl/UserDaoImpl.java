package com.bbs.dao.impl;

import com.bbs.dao.UserDao;
import com.bbs.entity.User;

public class UserDaoImpl extends BaseDao implements UserDao {
    /**
     * 查询用户
     * @param username 用户名
     * @return
     */
    @Override
    public User queryByUserName(String username) {
        String sql = "select userId,userName,userPass,head,gender,regTime from t_user where userName = ?";
        return queryone(User.class,sql,username);
    }

    /**
     * 注册成功，保存用户
     * @param user 用户
     * @return
     */
    @Override
    public int saveUser(User user) {
        String sql = "insert into t_user(userName,userPass,head,gender,regTime) values(?,?,?,?,?)";
        return update(sql,user.getUserName(),user.getUserPass(),user.getHead(),user.getGender(),
                user.getRegTime());
    }

    /**
     * 登录验证
     * @param username 用户名
     * @param password 密码
     * @return
     */
    @Override
    public User queryByUserNameAndPassword(String username, String password) {
        String sql = "select userId,userName,userPass,head,gender,regTime from t_user where userName = ? and userPass" +
                " = ?";
        return queryone(User.class,sql,username,password);
    }

    /**
     * 更改用户密码
     * @param password  密码
     * @param id 用户id
     * @return
     */
    @Override
    public int updateUserPassword( String password,Integer id) {
        String sql =  "update t_user set userPass = ? where userId = ?";
        return update(sql,password,id);
    }
}
