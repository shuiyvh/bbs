package com.bbs.dao.impl;

import com.bbs.dao.BoardDao;
import com.bbs.entity.Board;

import java.util.List;

public class BoardDaoImpl extends BaseDao implements BoardDao {
    @Override
    public int addBoard(Board board) {
        String sql = "insert into t_board(boardName) values(?)";
        return update(sql,board.getBoardName());
    }

    @Override
    public int deleteBoard(String boardName) {
        String sql = "delete from t_board where boardName = ?";
        return update(sql,boardName);
    }

    @Override
    public List<Board> queryBoard() {
        String sql = "select boardId,boardName,parentId from t_board";
        return queryList(Board.class,sql);
    }

    @Override
    public Object queryBoard(Integer boardId) {
        String sql = "select boardName from t_board where boardId = ?";
        return queryForSingleValue(sql,boardId);
    }
}
