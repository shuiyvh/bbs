package com.bbs.entity;

import java.util.Date;

public class Topic {
    private Integer topicId;
    private String title;
    private String content;
    private Date publishTime;
    private Date modifyTime;
    private Integer userId;
    private Integer boardId;

    public Topic() {
    }

    public Topic(Integer topicId, String title, String content, Date publishTime,
                 Date modifyTime, Integer userId, Integer boardId) {
        this.topicId = topicId;
        this.title = title;
        this.content = content;
        this.publishTime = publishTime;
        this.modifyTime = modifyTime;
        this.userId = userId;
        this.boardId = boardId;
    }

    public Integer getTopicId() {
        return topicId;
    }

    public void setTopicId(Integer topicId) {
        this.topicId = topicId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getBoardId() {
        return boardId;
    }

    public void setBoardId(Integer boardId) {
        this.boardId = boardId;
    }

    @Override
    public String toString() {
        return "Topic{" +
                "topicId=" + topicId +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", publishTime=" + publishTime +
                ", modifyTime=" + modifyTime +
                ", userId=" + userId +
                ", boardId=" + boardId +
                '}';
    }
}
