package com.bbs.entity;

public class Board {
    private Integer boardId;
    private String boardName;
    private Integer parentId;

    public Board() {
    }

    public Board(Integer boardId, String boardName, Integer parentId) {
        this.boardId = boardId;
        this.boardName = boardName;
        this.parentId = parentId;
    }

    public Integer getBoardId() {
        return boardId;
    }

    public void setBoardId(Integer boardId) {
        this.boardId = boardId;
    }

    public String getBoardName() {
        return boardName;
    }

    public void setBoardName(String boardName) {
        this.boardName = boardName;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    @Override
    public String toString() {
        return "Board{" +
                "boardId=" + boardId +
                ", boardName='" + boardName + '\'' +
                ", parentId=" + parentId +
                '}';
    }
}
