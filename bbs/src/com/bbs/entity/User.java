package com.bbs.entity;

import sun.net.www.content.image.gif;

import java.util.Date;

public class User {

    private Integer userId;
    private String userName;
    private String userPass;
    private String head= "image/head.jpeg";
    private String gender;
    private Date regTime;

    public User() {
    }

    public User(Integer userId, String userName, String userPass, String head, String gender, Date regTime) {
        this.userId = userId;
        this.userName = userName;
        this.userPass = userPass;
        if (head != null && !"".equals(head)) {
            this.head = head;
        }
        this.gender = gender;
        this.regTime = regTime;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getRegTime() {
        return regTime;
    }

    public void setRegTime(Date regTime) {
        this.regTime = regTime;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", userPass='" + userPass + '\'' +
                ", head='" + head + '\'' +
                ", gender='" + gender + '\'' +
                ", regTime=" + regTime +
                '}';
    }
}
