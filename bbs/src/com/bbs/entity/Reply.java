package com.bbs.entity;

import java.util.Date;

public class Reply {
    private Integer replyId;
    private String title;
    private String context;
    private Date ublishTime;
    private Date modifyTime;
    private Integer userId;
    private Integer topicId;

    public Reply() {
    }

    public Reply(Integer replyId, String title, String context, Date ublishTime,
                 Date modifyTime, Integer userId, Integer topicId) {
        this.replyId = replyId;
        this.title = title;
        this.context = context;
        this.ublishTime = ublishTime;
        this.modifyTime = modifyTime;
        this.userId = userId;
        this.topicId = topicId;
    }

    public Integer getReplyId() {
        return replyId;
    }

    public void setReplyId(Integer replyId) {
        this.replyId = replyId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public Date getUblishTime() {
        return ublishTime;
    }

    public void setUblishTime(Date ublishTime) {
        this.ublishTime = ublishTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getTopicId() {
        return topicId;
    }

    public void setTopicId(Integer topicId) {
        this.topicId = topicId;
    }

    @Override
    public String toString() {
        return "Reply{" +
                "replyId=" + replyId +
                ", title='" + title + '\'' +
                ", context='" + context + '\'' +
                ", ublishTime=" + ublishTime +
                ", modifyTime=" + modifyTime +
                ", userId=" + userId +
                ", topicId=" + topicId +
                '}';
    }
}
