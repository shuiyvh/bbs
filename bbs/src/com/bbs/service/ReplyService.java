package com.bbs.service;

import com.bbs.entity.Reply;

import java.util.List;

public interface ReplyService {
    /**
     *  新增回帖
     * @param reply 回复贴
     */
    public int addReply(Reply reply);

    /**
     * 查看某一帖子下的回帖
     * @param topicId 原帖id
     * @return 返回其下的所有回帖
     */
    public List<Reply> queryReplyBytopicId(Integer topicId);

    /**
     * 查看个人的回帖
     * @param userId 用户id
     * @return 返回个人的回帖
     */
    public List<Reply> queryReplyByuserId(Integer userId);

    /**
     * 删除个人回帖
     * @param replyId 回帖id
     * @param userId  个人id
     */
    public int deleteReply(Integer replyId,Integer userId);
}
