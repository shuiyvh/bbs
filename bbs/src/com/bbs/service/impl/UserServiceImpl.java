package com.bbs.service.impl;

import com.bbs.dao.UserDao;
import com.bbs.dao.impl.UserDaoImpl;
import com.bbs.entity.User;
import com.bbs.service.UserService;

import java.util.Date;

public class UserServiceImpl implements UserService {

    private UserDao userDao = new UserDaoImpl();

    @Override
    public User registUser(User user) {
        user = new User(user.getUserId(),user.getUserName(),user.getUserPass(),user.getHead(),user.getGender(),new Date());
        userDao.saveUser(user);
        return user;
    }

    @Override
    public User login(User user) {
        return userDao.queryByUserNameAndPassword(user.getUserName(),user.getUserPass());
    }

    @Override
    public boolean existsUserName(String username) {
        if (userDao.queryByUserName(username) == null ){
            return false;
        }
        return true;
    }




    @Override
    public void updateUserPassword(String password,Integer id) {
        userDao.updateUserPassword(password,id);
    }
}
