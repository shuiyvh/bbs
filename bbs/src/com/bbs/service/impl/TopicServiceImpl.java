package com.bbs.service.impl;

import com.bbs.dao.TopicDao;
import com.bbs.dao.impl.TopicDaoImpl;
import com.bbs.entity.Topic;
import com.bbs.service.TopicService;

import java.util.Date;
import java.util.List;

public class TopicServiceImpl implements TopicService {

    private TopicDao topicDao = new TopicDaoImpl();
    @Override
    public void addTopic(Topic topic) {
        topic = new Topic(topic.getTopicId(),topic.getTitle(),topic.getContent(),new Date(),new Date(),
                topic.getUserId(),topic.getBoardId());
        topicDao.addTopic(topic);
    }

    @Override
    public List<Topic> queryTopic(Integer boardId) {
        return topicDao.queryTopic(boardId);
    }

    @Override
    public void updateTopic(Topic topic) {
        topic = new Topic(topic.getTopicId(),topic.getTitle(),topic.getContent(),new Date(),new Date(),
                topic.getUserId(),topic.getBoardId());
        topicDao.updateTopic(topic);
    }

    @Override
    public void deleteTopic(Integer topicId, Integer userId) {
        topicDao.deleteTopic(topicId,userId);
    }

    @Override
    public List<Topic> queryTopicByUserId(Integer userId) {
        return topicDao.queryTopicByUserId(userId);
    }

    @Override
    public List<Topic> queryTopicByTopicId(Integer topicId) {
        return topicDao.queryTopicByTopicId(topicId);
    }
}
