package com.bbs.service.impl;

import com.bbs.dao.ReplyDao;
import com.bbs.dao.impl.ReplyDaoImpl;
import com.bbs.entity.Reply;
import com.bbs.service.ReplyService;

import java.util.List;

public class ReplyServiceImpl implements ReplyService {
    private ReplyDao replyDao = new ReplyDaoImpl();
    @Override
    public int addReply(Reply reply) {
        reply = new Reply(reply.getReplyId(),reply.getTitle(),reply.getContext(),reply.getUblishTime(),reply.getModifyTime(),
                reply.getUserId(),reply.getTopicId());
        return replyDao.addReply(reply);
    }

    @Override
    public List<Reply> queryReplyBytopicId(Integer topicId) {
        return replyDao.queryReplyBytopicId(topicId);
    }

    @Override
    public List<Reply> queryReplyByuserId(Integer userId) {
        return replyDao.queryReplyByuserId(userId);
    }

    @Override
    public int deleteReply(Integer replyId, Integer userId) {
        return replyDao.deleteReply(replyId,userId);
    }
}
