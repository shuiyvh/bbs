package com.bbs.service.impl;

import com.bbs.dao.BoardDao;
import com.bbs.dao.impl.BoardDaoImpl;
import com.bbs.entity.Board;
import com.bbs.service.BoardService;

import java.util.List;

public class BoardServiceImpl implements BoardService {
    private BoardDao boardDao = new BoardDaoImpl();

    @Override
    public int addBoard(Board board) {
        board = new Board(board.getBoardId(),board.getBoardName(),board.getParentId());
        return boardDao.addBoard(board);
    }

    @Override
    public void deleteBoard(String boardName) {
        boardDao.deleteBoard(boardName);
    }

    @Override
    public List<Board> queryBoard() {
        return boardDao.queryBoard();
    }

    @Override
    public Object queryBoard(Integer boardId) {
        return boardDao.queryBoard(boardId);
    }
}
