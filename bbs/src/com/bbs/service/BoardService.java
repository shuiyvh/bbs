package com.bbs.service;

import com.bbs.entity.Board;

import java.util.List;

public interface BoardService {
    /**
     * 增加版块
     * @param board 版块

     */
    public int addBoard(Board board);

    /**
     * 删除版块
     */
    public void deleteBoard(String boardName);

    /**
     * 查询版块
     * @return 版块
     */
    public List<Board> queryBoard();

    /**
     * 通过id查询当前所属模块
     * @param boardId 版块id
     * @return
     */
    public Object queryBoard(Integer boardId);
}
