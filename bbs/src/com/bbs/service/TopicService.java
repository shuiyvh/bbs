package com.bbs.service;

import com.bbs.entity.Topic;

import java.util.List;

public interface TopicService {

    /**
     *  发表帖子
     * @param topic 帖子
     */
    public void addTopic(Topic topic);

    /**
     * 查询某模块下的所有帖子
     * @param boardId 版块号
     * @return 返回所有帖子
     */
    public List<Topic> queryTopic(Integer boardId);

    /**
     * 更新帖子标题内容
     */
    public void updateTopic(Topic topic);

    /**
     * 删除帖子
     * @param topicId 主题id
     * @param userId 发帖人id
     */
    public void deleteTopic(Integer topicId,Integer userId);

    /**
     * 查询个人发帖（个人）
     * @param userId Integer 发布者id
     * @return 该id下所有帖子
     */
    public List<Topic> queryTopicByUserId(Integer userId);

    /**
     * 查看帖子
     * @param topicId 帖子id
     * @return 该id帖子的详情
     */
    public List<Topic> queryTopicByTopicId(Integer topicId);

}
