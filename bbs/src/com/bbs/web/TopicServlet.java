package com.bbs.web;

import com.bbs.entity.Board;
import com.bbs.entity.Topic;
import com.bbs.service.BoardService;
import com.bbs.service.TopicService;
import com.bbs.service.impl.BoardServiceImpl;
import com.bbs.service.impl.TopicServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public class TopicServlet extends BaseServlet {
    private TopicService topicService = new TopicServiceImpl();
    private BoardService boardService = new BoardServiceImpl();

    protected void queryTopic(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // 获取浏览器请求参数boardId
         String board = req.getParameter("boardId");
         // 将String转换为Integer
         Integer boardId = Integer.parseInt(board);
        //调用service方法获取topic对象
        List<Topic> topics = topicService.queryTopic(boardId);
        Object boards = boardService.queryBoard(boardId);
        System.out.println(boards);
        req.setAttribute("queryTopic",topics);
        req.setAttribute("boardName",boards);
        req.getRequestDispatcher("/pages/topic/querytopics.jsp").forward(req,resp);
    }


    protected void addTopic(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
         req.getRequestDispatcher("/pages/topic/addtopic.jsp").forward(req,resp);
    }

    protected void getTopic(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
         String boardId = req.getParameter("boardId");
         Integer boardIds = Integer.parseInt(boardId);
         String title = req.getParameter("title");
         String content = req.getParameter("content");
         String userId = req.getParameter("userId");
         Integer userIds = Integer.parseInt(userId);
         topicService.addTopic(new Topic(null,title,content,new Date(),new Date(),userIds,boardIds));
        resp.sendRedirect(req.getContextPath()+"/topicServlet?action=queryTopic&boardId="+boardId);
    }


    protected void queryTopicByUserId(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
            IOException {
        String topicId = req.getParameter("topicId");
        Integer topicIds = Integer.parseInt(topicId);
        List<Topic> topics = topicService.queryTopicByTopicId(topicIds);
        req.setAttribute("usertopic",topics);
        req.getRequestDispatcher("/pages/topic/readandreplytopic.jsp").forward(req,resp);


    }
}
