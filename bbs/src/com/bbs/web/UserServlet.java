package com.bbs.web;

import com.bbs.entity.User;
import com.bbs.service.UserService;
import com.bbs.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class UserServlet extends BaseServlet {

    private UserService userService = new UserServiceImpl();

    /**
     * 注册
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    protected void regist(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("被访问了");
        // 获取请求的参数
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String gender = req.getParameter("gender");
        String head = req.getParameter("head");

        //检查用户名是否可用
        if(userService.existsUserName(username)){
            //不可用 跳回注册页面
            System.out.println("用户名"+username+"已存在！");
            // 将错误信息和回显的表单项信息保存到request域中
            req.setAttribute("msg","用户名"+username+"已存在！");
            req.setAttribute("password",password);
            req.getRequestDispatcher("/pages/user/regist.jsp").forward(req,resp);
        }else{
            //可用，调用service保存到数据库
            User registUser = userService.registUser(new User(null, username, password, head, gender, null));
            // 获取当前登陆session对象
            HttpSession session = req.getSession();
            // 将用户名保存到session域中
            session.setAttribute("user",registUser);
            req.getRequestDispatcher("/pages/user/index.jsp").forward(req,resp);
        }

    }

    /**
     * 登录
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    protected void login(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 获取请求的参数
        String username = req.getParameter("username");
        String passwd = req.getParameter("password");

        //调用service方法处理业务逻辑
        User loginUser = userService.login(new User(null,username,passwd,null,null,null));
        if (loginUser != null){
            // 获取当前登陆session对象
            HttpSession session = req.getSession();
            // 将用户名保存到session域中
            session.setAttribute("user",loginUser);
            //登录成功跳回登陆成功页面
            req.getRequestDispatcher("/pages/user/index.jsp").forward(req,resp);
        }else{
            // 将错误信息和回显的表单项信息保存到request域中
            req.setAttribute("msg","用户名或密码错误");
            req.setAttribute("username",username);
            //登录失败跳回登陆页面
            req.getRequestDispatcher("/pages/user/login.jsp").forward(req,resp);
        }

    }

    /**
     * 注销
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    protected void logout(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 获取登录用户的session会话对象
        HttpSession session = req.getSession();
        // 立刻销毁session
        session.invalidate();
        // 重定向到未登陆首页
        resp.sendRedirect(req.getContextPath());
    }

    /**
     * 查询个人主页
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    protected void queryUser(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("用户查询自己");
    }
}
