package com.bbs.web;

import com.bbs.entity.Reply;
import com.bbs.service.ReplyService;
import com.bbs.service.impl.ReplyServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public class ReplyServlet extends BaseServlet{
    private ReplyService replyService = new ReplyServiceImpl();
    protected void addReply(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
         String title = req.getParameter("title");
         String context = req.getParameter("context");
         String userId = req.getParameter("userId");
         Integer userIds = Integer.parseInt(userId);
         String topicId = req.getParameter("topicId");
         Integer topicIds = Integer.parseInt(topicId);
         replyService.addReply(new Reply(null,title,context,new Date(),new Date(),userIds,topicIds));

        resp.sendRedirect(req.getContextPath()+"/replyServlet?action=queryReplyByTopicId&topicId="+topicIds);
    }


    protected void queryReplyByTopicId(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
            IOException {
        String topicId = req.getParameter("topicId");
        Integer topicIds = Integer.parseInt(topicId);
        List<Reply> replies = replyService.queryReplyBytopicId(topicIds);
        req.setAttribute("reply",replies);
        req.getRequestDispatcher("/pages/topic/reply.jsp").forward(req,resp);

    }
}
