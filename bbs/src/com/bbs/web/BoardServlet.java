package com.bbs.web;

import com.bbs.entity.Board;
import com.bbs.service.BoardService;
import com.bbs.service.impl.BoardServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class BoardServlet extends BaseServlet{
    private BoardService boardService = new BoardServiceImpl();


    protected void queryBoard(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("wobeifangwenle");
        List<Board> boards = boardService.queryBoard();
        System.out.println(boards);
        req.setAttribute("board",boards);
        req.getRequestDispatcher("/index/index.jsp").forward(req,resp);
    }
}
